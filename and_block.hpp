#ifndef __AND_BLOCK_HPP__
#define __AND_BLOCK_HPP__

#include "logic_vm.hpp"

/** 
 * És kapu tetszőleges számú be- és kimenettel. 
 * Több kimenet esetén valamennyi kimenet az összes bemenet ÉS kapcsolatát
 * adja.
 */
class and_block : public logic_vm::block {
    public:
       and_block(std::string const &id) : block(id) {
            events.push_back(
                [&] () { 
                    bool tmp=true;
                    for (auto &in: input) {
                        tmp = tmp & in.get_state();
                    }
                    for (auto &out: output)
                        out.set_state(tmp);
                }
            );
       };
       ~and_block() { }
};

#endif // __AND_BLOCK_HPP__
