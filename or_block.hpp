#ifndef __OR_BLOCK_HPP__
#define __OR_BLOCK_HPP__

#include "logic_vm.hpp"

/** 
 * Vagy kapu tetszőleges számú be- és kimenettel. 
 * Több kimenet esetén valamennyi kimenet az összes bemenet VAGY kapcsolatát
 * adja.
 */
class or_block : public logic_vm::block {
    public:
       or_block(std::string const &id) : block(id) {
            events.push_back(
                [&] () { 
                    bool tmp=true;
                    for (auto &in: input) {
                        tmp = tmp | in.get_state();
                    }
                    for (auto &out: output)
                        out.set_state(tmp);
                }
            );
       };
       ~or_block() { }
};

#endif // __OR_BLOCK_HPP__
