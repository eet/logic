# Logic

Egy nagyon egyszerű logikai szimulátor keretrendszer. Elsődlegesen oktatási
céllal készült, C++11-es nyelvi elemek használatával.

Fejlesztői dokumentáció Doxygen segítségével készíthető:

> doxygen Doxyfile

A generált html kimenet megtekinthető [itt](http://www.eet.bme.hu/~szalai/logic/docs).

# Fordítás

A fordításhoz C++11 kompatibilis fordító kell, gcc 4.8 vagy frissebb, clang 3.2
vagy frissebb. Visual Studio-val nem teszteltem.

> g++ -std=c++11 -Wall -W -Werror logic.cpp -o logic

# Kimenetek

A logikai szimuláció az std::cout-ra írja a szimuláció kimenetét regényes
formában, sok komment nem kell hozzá. Továbbá minden logikai VM generál egy
.dot fájlt. Ebből a _dot_ nevű command line tool segítségével lehet
,,kapcsolást'' generálni. Praktikus lehet, ha szeretnénk meggyőződni arról,
hogy azt a kapcsolást szimuláljuk amit valóban akarunk. A forrásban lévő kép
példa közül a ring oszcillátor a látványosabb kimenetű. 

Első közelítésben a logikai blokkok szögletes kerettel vannak ellátva, a portok
kerekkel.

> dot -Tpdf Ringoszcillator.dot -o Ringoszcillator.pdf

A _dot_ a Graphviz csomag része, minden elterjedt Linux disztróban bent van
önállóan, Windows alatt a Graphvizt kell felvarázsolni. 
