#ifndef __LOGIC_VM_HPP__
#define __LOGIC_VM_HPP__

#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <functional>
#include <utility>

/**
 * Ez az osztály valósítja meg a logikai szimulátort. Ezt példányosítva  tudunk
 * "kapcsolásokat" létrehozni. A példányokba kell beregisztrálnunk az
 * áramkörünk elemeit, valamint azokat összekötni.
 */
class logic_vm {
    public:
        /**
         * Minden áramköri elem őse.
         */
        class block {
            public:
                /**
                 * Az áramköri elemek portjai. A portok meghajthatnak
                 * tetszőleges számú másik portot. Ekkor ha a portunk állapota
                 * megváltozik, az megváltoztatja a meghajtott portok állapotát
                 * is.
                 */
                class port {
                    private:
                        /** Port neve. */
                        std::string name;
                        /** Port aktuális állapota. */
                        bool state;
                        /** Meghajtott portok pointereinek vektora. */
                        std::vector<port*> driven;
                        /** A szülő áramköri elem pointere. */
                        block* owner;
                        /** Inicializáltság flag. A port első használatáig
                         * true. */
                        bool uninit;
                    public:
                        port(std::string const &name = "port default", bool state = false, block *owner = nullptr, bool uninit=true) : name(name), state(state), owner(owner), uninit(uninit) {
                        }
                        /** 
                         * Port állapotának megváltoztatása. Ez a függvény
                         * végzi az áramkörhöz tartozó művelet végrehajtási
                         * listába illesztését.
                         */
                        void set_state(bool new_state) {
                            if ((state != new_state) || uninit) {
                                uninit=false;
                                if(owner!=nullptr) {
                                    if(owner->parent!=nullptr) {
                                        std::cout << owner->parent->get_name() << ".";
                                    }
                                    std::cout << owner->get_id() << ".";
                                }
                                std::cout << name << ": " << state << " -> " << new_state << std::endl;  
                                state = new_state;
                                if(owner!=nullptr && driven.size()==0) {
                                    if (owner->parent != nullptr) {
                                        owner->parent->queue.push_back(std::make_pair(owner->parent->get_time() + owner->get_delay(), [&] () {owner->eval(); } )); 
                                    }
                                    else {
                                        throw "";
                                    }
                                }
                                if(driven.size()>0) {
                                    for(auto &drive: driven) {
                                        drive->set_state(state);
                                    }
                                }
                            }
                        }
                        /** Port nevének megváltoztatására. */
                        void set_name(std::string const &new_name) {
                            name = new_name;
                        }
                        /** Visszaadja a port gazdáját. */
                        block* get_owner() { return owner; }
                        /** 
                         * Állapot lekérdezése. 
                         * 
                         * @return bool állapot
                         */
                        bool get_state() { return state; }
                        /**
                         * Port nevének lekérdezése.
                         *
                         * @return port neve
                         */
                        std::string get_name() { return name; }
                        /**
                         * Meghajtott port hozzárendelése.
                         *
                         * @param p meghajtott port referenciája.
                         */
                        void add_driven(port &p) {
                            driven.push_back(&p);
                            owner->parent->dot_file << owner->id << name << "->" << p.owner->id << p.name << "\n";
                        }
                        /** Port nevének és állapotának a kiírása. */
                        void print() {
                            std::cout << "\t" << name << " -> " << state;
                            //                    if(owner!=nullptr)
                            //                        std::cout << "\t Owner: " << owner->id ;
                            //                    std::cout << std::endl;
                        }
                        ~port() {}
                };
            protected:
                /** Az áramköri elem azonosítója. */
                std::string id;
                /** Az áramkör bemeneti portjainak vektora. */
                std::vector<port> input;
                /** Az áramkör kimeneti portjainak vektora. */
                std::vector<port> output;
                /** Az áramkör viselkedését megadó események tárolója. */
                std::vector<std::function<void()> > events;
                /** Az áramköri elem késleltetése. */
                unsigned delay;
                /** A szülő logikai szimulátorra mutató pointer. */
                logic_vm *parent;
            public:
                block(std::string const &id = "block default", unsigned delay=1, logic_vm *parent=nullptr) : id(id), delay(delay), parent(parent) {};
                /** 
                 * Azonosító beállítása. 
                 *
                 * @param tag azonosító
                 */
                void set_id(std::string const &tag) {
                    id = tag;
                }
                /**
                 *  Azonosító lekérdezése.
                 *
                 *  @return azonosító
                 */
                std::string get_id() { return id; }
                /**
                 *  Port hozzáadása az áramkörhöz.
                 *
                 * @param name port neve
                 * @param type port típusa
                 */
                void add_port(std::string const &name, std::string type) {
                    parent->dot_file << id << name << " [label=\"" << name << "\" shape=\"circle\"]\n";
                    if(type=="input") {
                        input.push_back(port(name,false,this));
                        parent->dot_file << id << name << "->" << id << "\n";
                    }
                    else {
                        output.push_back(port(name,false,this));
                        parent->dot_file << id << "->" << id << name << "\n";
                    }
                }
                /**
                 * Késleltetés lekérdezése.
                 *
                 * @return delay
                 */
                unsigned get_delay() { return delay; }
                /**
                 * Port állapotának beállítása.
                 *
                 * @param name a port neve
                 * @param value a beállítani kívánt érték
                 */
                void set_port(std::string const &name, bool value) {
                    for(auto &p: input) {
                        if(p.get_name() == name) {
                            p.set_state(value);
                        }
                    }
                }
                /**
                 * Port kikeresése a neve alapján.
                 *
                 * @name port neve
                 * @return az adott portra mutató referencia
                 */
                port & get_port(std::string const& name){
                    for(auto &in: input) 
                        if(in.get_name() == name)
                            return in;
                    for(auto &out: output)
                        if(out.get_name() == name)
                            return out;
                    throw "";
                }
                /**
                 * Portok összekapcsolása.
                 *
                 * @param out meghajtó port
                 * @param b meghajtani kívánt áramkör
                 * @param in meghajtani kívánt port
                 */
                void connect(std::string const &out, block &b, std::string const &in) {
                    this->get_port(out).add_driven(b.get_port(in));

                    std::cout << "Connect ";
                    if(this->parent != nullptr)
                        std::cout << this->parent->get_name() << ".";
                    std::cout << this->id << "." << get_port(out).get_name() << " -> ";
                    if(b.parent != nullptr)
                        std::cout << b.parent->get_name() << ".";
                    std::cout << b.id << "." << b.get_port(in).get_name() << std::endl;
                }
                /**
                 * Az áramkör működésének aktiválása. Ez a függvény valamennyi az
                 * áramkörhöz tartozó eseményt végrehajtja.
                 */
                void eval() {
                    for(auto gyi: events) {
                        gyi();
                    }
                }
                /**
                 * Infók kiírása.
                 */
                void print() {
                    std::cout << "ID: " << id << std::endl;
                    std::cout << "Delay: " << delay << std::endl;
                    std::cout << "Input(s): ";
                    for(auto &in: input) in.print();
                    std::cout << std::endl;
                    std::cout << "Output(s): ";
                    for(auto &out: output) out.print();
                    std::cout << std::endl;
                    if (parent != nullptr) 
                        std::cout << "Parent: " << parent->get_name() << std::endl;
                }
                /** 
                 * Szülő logikai szimulátor beállítása.
                 *
                 * @param p szülőre mutató pointer
                 */
                void set_parent(logic_vm *p) { parent = p; }
                virtual ~block() { }

        };
    private:
        /** A logikai szimulátor aktuális idejét tartalmazó változó. */
        unsigned time_stamp=0;
        /** A szimulátor példány neve. */
        std::string vm_name;
    protected:
        /** 
         * Feladatlista. Tartalmazza a végrehajtandó eseményt és annak az
         * időpontját.
         */
        std::list<std::pair<unsigned, std::function<void()> > > queue;
        /** Dot file. */
        std::ofstream dot_file;
    public:
        /** A szimulátorhoz adott áramköri elemek pointerei. */
        std::list<block*> elements;
        logic_vm(std::string const &vm_name="Default-VM") : vm_name(vm_name) { 
            std::cout << "Logic VM " << vm_name << " has been created." << std::endl; 
            dot_file.open(vm_name+".dot");
            dot_file << "digraph " << vm_name << " {\n";
        }
        /**
         * Áramköri elem hozzáadása.
         *
         * @param element az áramkörre mutató pointer
         */
        void add(block *element) {
            element->set_parent(this);
            elements.push_back(element);
            dot_file << element->get_id() << " [label=\"" << element->get_id() << "\" shape=\"box\"]" << "\n";
        }

        /**
         * Áramköri elem keresése azonosító alapján.
         *
         * @param id azonosító
         * @return az elemre mutató referencia
         */
        block& element(std::string const &id) {
            for( auto &element: elements)
                if (element->get_id() == id)
                    return *element;
            throw "";
        }

        /** Logikai szimulátor nevének lekérdezése. */
        std::string get_name() { return vm_name; }
        /** Logikai szimulátor aktuális idejének lekérdezése. */
        unsigned get_time() { return time_stamp; }
        /** Statisztikák kiírása. */
        void info() {
            std::cout << "    VM informations:" << std::endl;
            std::cout << "\t vm_name: \t\t" << vm_name << std::endl;
            std::cout << "\t time_stamp: \t\t" << time_stamp << std::endl;
            std::cout << "\t Elements in the VM: \t" << elements.size() << std::endl;
            std::cout << "\t Events in the queue: \t" << queue.size() << std::endl;
        }

        /** 
         * Szimuláció futtatása. A szimuláció előre definiált ideig fut.
         *
         * @param end a kívánt időlépések száma
         */
        void run(unsigned end) {
            unsigned until = time_stamp+end;
            for(; time_stamp<until; ++time_stamp) {
                while(queue.front().first == time_stamp) {
                    std::cout << "Event at " << time_stamp << std::endl;
                    (queue.front().second)();
                    queue.pop_front();
                }
            }
        }

        /**
         * A végrehajtási lista kiírása
         */
        void list_queue() {
            for(auto instance: queue) {
                std::cout << "Time: " << instance.first << std::endl;
            }
        }

        ~logic_vm() {
            dot_file << "}\n";
            dot_file.close();
            for (auto element: elements) {
                delete element;
            }
            std::cout << "Logic VM " << vm_name << " has been destructed." << std::endl; 
        }
};

#endif // __LOGIC_VM_HPP__
