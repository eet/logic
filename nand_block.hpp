#ifndef __NAND_BLOCK_HPP__
#define __NAND_BLOCK_HPP__

#include "logic_vm.hpp"

/** 
 * Nem-És kapu tetszőleges számú be- és kimenettel. 
 * Több kimenet esetén valamennyi kimenet az összes bemenet Nem-És kapcsolatát
 * adja.
 */
class nand_block : public logic_vm::block {
    public:
       nand_block(std::string const &id) : block(id) {
            events.push_back(
                [&] () { 
                    bool tmp=true;
                    for (auto &in: input) {
                        tmp = tmp & in.get_state();
                    }
                    for (auto &out: output)
                        out.set_state(!tmp);
                }
            );
       };
       ~nand_block() { }
};

#endif // __NAND_BLOCK_HPP__

