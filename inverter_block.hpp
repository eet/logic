#ifndef __INVERTER_BLOCK_HPP__
#define __INVERTER_BLOCK_HPP__

#include "logic_vm.hpp"

/** 
 * Inverter tetszőleges egy be- és tetszőleges számú kimenetre. 
 * Több kimenet esetén valamennyi kimenet a bemenet negáltját
 * adja.
 */
class inverter_block : public logic_vm::block {
    public:
       inverter_block(std::string const &id) : block(id) {
            events.push_back(
                [&] () { 
                    for (auto &out: output)
                        out.set_state(!input[0].get_state());
                }
            );
       };
       ~inverter_block() { }
};

#endif // __INVERTER_BLOCK_HPP__

