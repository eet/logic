#include <iostream>
#include <list>
#include <vector>
#include <functional>
#include "and_block.hpp"
#include "nand_block.hpp"
#include "or_block.hpp"
#include "inverter_block.hpp"
#include "logic_vm.hpp"

int main() {

    // Simple Flip-Flop
    logic_vm VM("FlipFlop");

    VM.add(new nand_block("nand1"));
    VM.element("nand1").add_port("A","input");
    VM.element("nand1").add_port("B","input");
    VM.element("nand1").add_port("Y1","output");
    
    VM.add(new nand_block("nand2"));
    VM.element("nand2").add_port("C","input");
    VM.element("nand2").add_port("D","input");
    VM.element("nand2").add_port("Y2","output");
   
    VM.element("nand1").connect("Y1", VM.element("nand2"), "C");
    VM.element("nand2").connect("Y2", VM.element("nand1"), "B");
    VM.element("nand1").set_port("A",true);
    VM.run(6);
    VM.element("nand1").set_port("A",false);
    VM.element("nand1").set_port("D",true);
    VM.run(6);
    VM.info();

    // Ring osc.
    logic_vm VM2("Ringoszcillator");
    VM2.add(new inverter_block("inv1"));
    VM2.element("inv1").add_port("IN","input");
    VM2.element("inv1").add_port("OUT","output");
    VM2.add(new inverter_block("inv2"));
    VM2.element("inv2").add_port("IN","input");
    VM2.element("inv2").add_port("OUT","output");
    VM2.add(new inverter_block("inv3"));
    VM2.element("inv3").add_port("IN","input");
    VM2.element("inv3").add_port("OUT","output");
    VM2.element("inv1").connect("OUT", VM2.element("inv2"), "IN");
    VM2.element("inv2").connect("OUT", VM2.element("inv3"), "IN");
    VM2.element("inv3").connect("OUT", VM2.element("inv1"), "IN");

    VM2.element("inv1").set_port("IN",true);
    VM2.run(10);
    VM2.info();

    return 0;
}
